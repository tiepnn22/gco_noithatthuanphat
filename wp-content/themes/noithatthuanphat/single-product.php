<?php get_header(); ?>

<?php
    $product_id = get_the_ID();

	//$glb_tax_product biến toàn cục
	$terms = wp_get_object_terms($post->ID, $glb_tax_product);
	if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $term = $terms[0];
	$term_id = $term->term_id;
	$term_name = $term->name;

    //info product
    $single_product_title   = get_the_title($product_id);
    $single_product_content = wpautop(get_the_content($product_id));
    $single_product_date    = get_the_date('d/m/Y', $product_id);
    $single_product_link    = get_post_permalink($product_id);
    $single_product_image   = getPostImage($product_id,"p-product");
    $single_product_excerpt = cut_string(get_the_excerpt($product_id),300,'...');
    $single_recent_author   = get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
    $single_product_author  = $single_recent_author->display_name;
    $single_product_tag     = get_the_tags($product_id);

    //link page contact
    $pages = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => 'template-contact.php'
    ));
    $page_contact_link = get_page_link($pages[0]->ID);

    //field
    $h_top_phone = get_field('h_top_phone', 'option');
    $s_p_gallery = get_field('s_p_gallery');
    $s_p_detail = get_field('s_p_detail');
?>

<style>.single-product-detail .single-product-detail-content .product-detail-content *{padding:5px 0}</style>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="single-product">
    <div class="container">
        <div class="single-product-content">
            <div class="row">

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 single-product-gallery">
                    <div class="gallery-full slider-for-p">
                        
                        <a href="javascript:void(0)">
                            <img src="<?php echo asset('images/3x2.png'); ?>" style="background-image: url('<?php echo $single_product_image; ?>')">
                        </a>

                        <?php if(!empty( $s_p_gallery )) { ?>
                        <?php
                            foreach ($s_p_gallery as $s_p_gallery_kq) {
                        ?>

                            <a href="javascript:void(0)">
                                <img src="<?php echo asset('images/3x2.png'); ?>" style="background-image: url('<?php echo $s_p_gallery_kq; ?>')">
                            </a>

                        <?php } ?>
                        <?php } ?>

                    </div>
                    <div class="gallery-thumbs">
                        <ul class="slider-nav-p">

                            <?php if(!empty( $s_p_gallery )) { ?>
                            <li>
                                <a href="javascript:void(0)">
                                    <img src="<?php echo asset('images/3x2.png'); ?>" style="background-image: url('<?php echo $single_product_image; ?>')">
                                </a>
                            </li>

                            
                            <?php
                                foreach ($s_p_gallery as $s_p_gallery_kq) {
                            ?>

                                <li>
                                    <a href="javascript:void(0)">
                                        <img src="<?php echo asset('images/3x2.png'); ?>" style="background-image: url('<?php echo $s_p_gallery_kq; ?>')">
                                    </a>
                                </li>

                            <?php } ?>
                            <?php } ?>

                        </ul>
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 single-product-info">
                    <div class="entry-title-top">
                        <h1 class="entry-title">
                            <?php echo $single_product_title; ?>
                        </h1>
                        <div class="entry-socical">
                            <div class="fb-like" data-href="<?php the_permalink(); ?>" data-width="" data-layout="button_count" data-action="like" data-size="small" data-share="true"></div>
                        </div>
                    </div>
                    <div class="entry-price">
                        <?php
                            //show_price
                            $data_show_price = array(
                                'post_id'     =>    $product_id
                            );
                            get_template_part("resources/views/wc-product-price",$data_show_price);
                        ?>
                    </div>
                    <div class="entry-desc">
                        <?php echo wpautop( $s_p_detail ); ?>
                    </div>
                    <div class="entry-button">
                        <a class="entry-button-call" href="tel:<?php echo str_replace(' ','',$h_top_phone);?>">Gọi ngay</a>
                        <a class="entry-button-buy" href="<?php echo $page_contact_link; ?>">Đặt hàng</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="single-product-detail">
    <div class="container">
        <div class="row">

            <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 col-12 single-product-detail-content">
                <div class="product-detail-title">
                    <h2>Chi tiết sản phẩm</h2>
                </div>
                <div class="product-detail-content">
                    <?php echo $single_product_content; ?>
                </div>
                <?php get_template_part("resources/views/social-bar"); ?>
            </div>

            <?php
                // get_sidebar();
            ?>
            <?php get_template_part("resources/views/template-related-product"); ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>