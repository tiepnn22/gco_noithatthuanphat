<?php
// Register Style
function theme_Style() {
    wp_register_style( 'tool-style', get_stylesheet_directory_uri() . "/dist/css/tool.min.css",false, 'all' );
    wp_enqueue_style('tool-style');
    wp_register_style( 'main-style', get_stylesheet_directory_uri() . "/dist/css/main.css",false, 'all' );
    wp_enqueue_style('main-style');

    wp_register_script( 'tool-script', get_stylesheet_directory_uri() . "/dist/js/tool.min.js", array('jquery'),false,true );
    wp_enqueue_script('tool-script');
    wp_register_script( 'main-script', get_stylesheet_directory_uri() . "/dist/js/main.js", array('jquery'),false,true );
    wp_enqueue_script('main-script');
}
if (!is_admin()) add_action('wp_enqueue_scripts', 'theme_Style');


// Register Menu and image
if (!function_exists('theme_Setup')) {
    function theme_Setup()
    {
        register_nav_menus( array(
            'primary' => __( 'Menu chính', 'text_domain' ),
        ) );

        // add_theme_support( 'woocommerce' );
	    add_theme_support( 'post-thumbnails' );
	    add_image_size( 'p-product', 600, 400, true );
	    add_image_size( 'p-post', 350, 250, true );
    }
    add_action('after_setup_theme', 'theme_Setup');
}


// Register Sidebar
if (!function_exists('theme_Widgets')) {
    function theme_Widgets()
    {
        $sidebars = [
            [
				'name'          => __( 'Vùng quảng cáo', 'text_domain' ),
				'id'            => 'adv-ads',
				'description'   => __( 'Vùng quảng cáo dưới chân trang', 'text_domain' ),
				'before_widget' => '<div id="%1$s" class="widget col-lg-6 col-md-6 col-sm-6 col-xs-6 %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }
    add_action('widgets_init', 'theme_Widgets');
}


// Shortcode play on Widget
add_filter('widget_text','do_shortcode');
// Use Block Editor default for Post
// add_filter('use_block_editor_for_post', '__return_false');


// Delete class and id of wp_nav_menu()
function wp_nav_menu_attributes_filter($var) {
    return is_array($var) ? array_intersect($var, array('current-menu-item','mega-menu')) : '';
}
add_filter('nav_menu_css_class', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('nav_menu_item_id', 'wp_nav_menu_attributes_filter', 100, 1);
add_filter('page_css_class', 'wp_nav_menu_attributes_filter', 100, 1);


// Delete span, br on Contact Form 7
add_filter('wpcf7_form_elements', function($content) {
    // Delete span (ko xoá đc vì sẽ mất thông báo validate)
    // $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);
    // Delete br
    $content = str_replace('<br />', '', $content);
    return $content;
});

// Remove Contact Form 7 script and css, add call funtion in contact.php
add_filter( 'wpcf7_load_js', '__return_false' );
add_filter( 'wpcf7_load_css', '__return_false' );

// Call funtion in contact.php
// if ( function_exists( 'wpcf7_enqueue_scripts' ) ) { wpcf7_enqueue_scripts(); }
// if ( function_exists( 'wpcf7_enqueue_styles' ) ) { wpcf7_enqueue_styles(); }

// Setup SMTP
function setup_smtp_email( $phpmailer ) {
    $phpmailer->isSMTP();     
    $phpmailer->Host        = get_field('smtp_host', 'option');
    $phpmailer->Port        = get_field('smtp_port', 'option');
    $phpmailer->SMTPAuth    = get_field('smtp_auth', 'option');
    $phpmailer->Username    = get_field('smtp_user', 'option');
    $phpmailer->Password    = get_field('smtp_pass', 'option');
    $phpmailer->SMTPSecure  = get_field('smtp_encryption', 'option');
}
add_action( 'phpmailer_init', 'setup_smtp_email' );





// Query_post_by_custompost
function query_post_by_custompost($posttype_name, $numPost){
    $qr =  new WP_Query( array(
                        'post_type'     => $posttype_name,
                        'showposts'     => $numPost,
                        'order'         => 'DESC',
                        'orderby'       => 'date'
                ) );
    return $qr;
}
function query_post_by_custompost_paged($posttype_name, $numPost){
    $qr =  new WP_Query( array(
                        'post_type'     => $posttype_name,
                        'showposts'     => $numPost,
                        'order'         => 'DESC',
                        'orderby'       => 'date',
                        'paged'         => (get_query_var('paged')) ? get_query_var('paged') : 1
                ) );
    return $qr;
}


// Query_post_by_category
function query_post_by_category($cat_id, $numPost){
    $qr =  new WP_Query( array(
                        'cat'           => $cat_id,
                        'showposts'     => $numPost,
                        'order'         => 'DESC',
                        'orderby'       => 'date'
                ) );
    return $qr;
}
function query_post_by_category_paged($cat_id, $numPost){
    $qr =  new WP_Query( array(
                        'cat'           => $cat_id,
                        'showposts'     => $numPost,
                        'order'         => 'DESC',
                        'orderby'       => 'date',
                        'paged'         => (get_query_var('paged')) ? get_query_var('paged') : 1
                ) );
    return $qr;
}


// Query_post_by_tag
function query_post_by_tag($tag_id, $numPost){
    $qr =  new WP_Query( array(
                        'tag_id'        => $tag_id,
                        'showposts'     => $numPost,
                        'order'         => 'DESC',
                        'orderby'       => 'date'
                ) );
    return $qr;
}
function query_post_by_tag_paged($tag_id, $numPost){
    $qr =  new WP_Query( array(
                        'tag_id'        => $tag_id,
                        'showposts'     => $numPost,
                        'order'         => 'DESC',
                        'orderby'       => 'date',
                        'paged'         => (get_query_var('paged')) ? get_query_var('paged') : 1
                ) );
    return $qr;
}


// Query_post_by_taxonomy
function query_post_by_taxonomy($posttype_name, $taxonomy_name, $term_id, $numPost){
    $qr =  new WP_Query( array(
                        'post_type'     =>  $posttype_name,
                        'tax_query'     =>  array(
                                                array(
                                                        'taxonomy'  => $taxonomy_name,
                                                        'field'     => 'id',
                                                        'terms'     => $term_id,
                                                        'operator'  => 'IN'
                                                )),
                        'showposts'     =>  $numPost,
                        'order'         =>  'DESC',
                        'orderby'       =>  'date'
                ) );
    return $qr;
}
function query_post_by_taxonomy_paged($posttype_name, $taxonomy_name, $term_id, $numPost){
    $qr =  new WP_Query( array(
                        'post_type'     =>  $posttype_name,
                        'tax_query'     =>  array(
                                                array(
                                                        'taxonomy'  => $taxonomy_name,
                                                        'field'     => 'id',
                                                        'terms'     => $term_id,
                                                        'operator'  => 'IN'
                                                )),
                        'showposts'     =>  $numPost,
                        'order'         =>  'DESC',
                        'orderby'       =>  'date',
                        'paged'         =>  (get_query_var('paged')) ? get_query_var('paged') : 1
                ) );
    return $qr;
}


// Query_page
function query_page(){
    $qr =  new WP_Query( array(
                        'post_type'      => 'page',
                        'showposts'      => -1,
                        'order'          => 'ASC',
                        'orderby'        => 'menu_order'
                ) );
    return $qr;
}


// Query_page_by_page_parent
function query_page_by_page_parent($page_id){
    $qr =  new WP_Query( array(
                        'post_type'      => 'page',
                        'post_parent'    => $page_id,
                        'showposts'      => -1,
                        'order'          => 'ASC',
                        'orderby'        => 'menu_order'
                ) );
    return $qr;
}


// Query_search_post
function query_search_post($keyword, $posttype_array, $numPost){
    $qr =  new WP_Query( array(
                        's'              => $keyword,
                        'post_type'      => $posttype_array,
                        'showposts'      => $numPost,
                        'order'          => 'DESC',
                        'orderby'        => 'type'
                ) );
    return $qr;
}
function query_search_post_paged($keyword, $posttype_array, $numPost){
    $qr =  new WP_Query( array(
                        's'              => $keyword,
                        'post_type'      => $posttype_array,
                        'showposts'      => $numPost,
                        'order'          => 'DESC',
                        'orderby'        => 'type',
                        'paged'          => (get_query_var('paged')) ? get_query_var('paged') : 1
                ) );
    return $qr;
}


// Query_post_views_hot
function query_post_views_hot($numPost){
    $qr =  new WP_Query( array(
                        'meta_key'       => 'post_views_count',
                        'showposts'      => $numPost,
                        'order'          => 'DESC',
                        'orderby'        => 'meta_value_num'
                ) );
    return $qr;
}
function query_post_views_hot_paged($numPost){
    $qr =  new WP_Query( array(
                        'meta_key'       => 'post_views_count',
                        'showposts'      => $numPost,
                        'order'          => 'DESC',
                        'orderby'        => 'meta_value_num',
                        'paged'          => (get_query_var('paged')) ? get_query_var('paged') : 1
                ) );
    return $qr;
}


// Query_post_buy_hot
function query_post_buy_hot($posttype_name, $numPost){
    $qr =  new WP_Query( array(
                        'post_type'      => $posttype_name,
                        'meta_key'       => 'total_sales',
                        'showposts'      => $numPost,
                        'order'          => 'DESC',
                        'orderby'        => 'meta_value_num'
                ) );
    return $qr;
}
function query_post_buy_hot_paged($posttype_name, $numPost){
    $qr =  new WP_Query( array(
                        'post_type'      => $posttype_name,
                        'meta_key'       => 'total_sales',
                        'showposts'      => $numPost,
                        'order'          => 'DESC',
                        'orderby'        => 'meta_value_num',
                        'paged'          => (get_query_var('paged')) ? get_query_var('paged') : 1
                ) );
    return $qr;
}



// Edit Footer on Admin
if (!function_exists('remove_footer_admin')) {
    function remove_footer_admin () {
        echo 'Thiết kế website bởi <a href="gco.vn" target="_blank">GCO</a>';
    }
    add_filter('admin_footer_text', 'remove_footer_admin');
}


// Close all Update
if (!function_exists('remove_core_updates')) {
    function remove_core_updates() {
        global $wp_version;
        return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
    }
    add_filter('pre_site_transient_update_core','remove_core_updates');
    add_filter('pre_site_transient_update_plugins','remove_core_updates');
    add_filter('pre_site_transient_update_themes','remove_core_updates');
}


// Custom Favicon
if (!function_exists('custom_favicon')) {
    function custom_favicon() {
        echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_field('h_logo', 'option').'" />';
    }
    add_action('wp_head', 'custom_favicon');
}


// Custom logo admin bar
if (!function_exists('wpb_custom_logo')) {
    function wpb_custom_logo() {
        echo '
            <style type="text/css">
                #wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon::before {
                    background-image: url('.get_field('h_logo', 'option').');
                    background-position: left center;
                    background-repeat: no-repeat;
                    background-size: contain;
                    -moz-background-size: contain;
                    -webkit-background-size: contain;
                    -o-background-size: contain;
                    -ms-background-size: contain;
                    content: "";
                    display: inline-block;
                    height: 20px;
                    left: 3px;
                    top: 3px;
                    width: 25px;
                }
            </style>';
    }
    add_action('wp_before_admin_bar_render', 'wpb_custom_logo');
}


// Custom logo admin login
if (!function_exists('my_login_logo_one')) {
    function my_login_logo_one() {
        echo '
            <style type="text/css">
                body.login div#login h1 a {
                    background-image: url('.get_field('h_logo', 'option').');
                    background-position: center center;
                    background-repeat: no-repeat;
                    background-size: contain;
                    -moz-background-size: contain;
                    -webkit-background-size: contain;
                    -o-background-size: contain;
                    -ms-background-size: contain;
                    display: inline-block;
                    height: 200px;
                    line-height: 0;
                    margin: -30px auto 0;
                    width: 200px;
                }
            </style>';
    }
    add_action( 'login_enqueue_scripts', 'my_login_logo_one' );
}


// Remove WP Version Css Js
function remove_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
if (!is_admin()) add_filter( 'style_loader_src', 'remove_ver_css_js', 9999 );
if (!is_admin()) add_filter( 'script_loader_src', 'remove_ver_css_js', 9999 );


// Add defer to the Css Js
function add_async_attribute_css($tag, $handle) {
    // $scripts_to_async = array('tool-style', 'main-style');
    // foreach($scripts_to_async as $async_script) {
        // if ($async_script === $handle) {
            return str_replace(' href', ' async="async" href', $tag);
        // }
    // }
    return $tag;
}
function add_async_attribute_js($tag, $handle) {
    // $scripts_to_async = array('tool-script', 'main-script');
    // foreach($scripts_to_async as $async_script) {
        // if ($async_script === $handle) {
        if ($handle != 'jquery-core' ) {
            return str_replace(' src', ' defer="defer" src', $tag);
        }
        // }
    // }
    return $tag;
}
if (!is_admin()) add_filter('style_loader_tag', 'add_async_attribute_css', 10, 2);
if (!is_admin()) add_filter('script_loader_tag', 'add_async_attribute_js', 10, 2);


// Remove JQuery migrate
function remove_jquery_migrate( $scripts ) {
    if ( !is_admin() && isset( $scripts->registered['jquery'] ) ) {
        $script = $scripts->registered['jquery'];
        if ( $script->deps ) { 
            $script->deps = array_diff( $script->deps, array( 'jquery-migrate' ) );
        }
    }
}
add_action( 'wp_default_scripts', 'remove_jquery_migrate' );


//* Adding DNS Prefetching
// function wp_dns_prefetch() {
//     echo '
//     <meta http-equiv="x-dns-prefetch-control" content="on">
//     <link rel="dns-prefetch" href="//youtube.com" />
//     <link rel="dns-prefetch" href="//connect.facebook.net" />
//     ';
// }
// add_action('wp_head', 'wp_dns_prefetch', 0);


// Disable wordpress Embeds
function my_deregister_scripts(){
    wp_dequeue_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );


/**
 * Disable the emoji's
 */
function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
    add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param array $plugins 
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
    if ( 'dns-prefetch' == $relation_type ) {
        /** This filter is documented in wp-includes/formatting.php */
        $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

        $urls = array_diff( $urls, array( $emoji_svg_url ) );
    }
    return $urls;
}
?>