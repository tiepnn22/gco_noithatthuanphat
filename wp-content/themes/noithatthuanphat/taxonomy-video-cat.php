<?php get_header(); ?>

<?php
	//$glb_ctp_video biến toàn cục
	//$glb_tax_video biến toàn cục
	$term_info 		= get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
	$term_id 		= $term_info->term_id;
	$term_name 		= $term_info->name;
	$term_excerpt 	= wpautop($term_info->description);
	$term_link 		= esc_url(get_term_link($term_id));
	$taxonomy_name 	= $term_info->taxonomy;
?>

<?php get_template_part("resources/views/page-banner"); ?>

<h1 style="display: none;">Video</h1>
<h2 style="display: none;">Nội thất Thuận Phát</h2>

<section class="page-video">
    <div class="container">

		<div class="page-video-content">
            <div class="row">

				<?php
					$query = query_post_by_taxonomy_paged($glb_ctp_video, $taxonomy_name, $term_id, 9);
					// $max_num_pages = $query->max_num_pages;

					if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

                    $post_id 		= get_the_ID();
                    $post_title 	= get_the_title($post_id);
                    $post_content 	= wpautop(get_the_content($post_id));
                    $post_date 		= get_the_date('Y/m/d',$post_id);
                    $post_link 		= get_post_permalink($post_id);
                    $post_image 	= getPostImage($post_id,"p-product");
                    $post_excerpt 	= cut_string(get_the_excerpt($post_id),80,'...');
                    $post_author 	= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
                    $post_tag 		= get_the_tags($post_id);

					$video_url 		= getIframeVideo( get_field('video_url', $post_id) );
					$oldprice 		= get_field('video_price_regular', $post_id);
					$donvi 			= " đ";
					$price 			= '';
					$video_view_check = get_field('video_view', $post_id);
					$video_view 	= (!empty($video_view_check)) ? $video_view_check : '0';
				?>

					<article class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
						<div class="item" data-toggle="modal" data-target="#Modal1" data-video="<?php echo $video_url; ?>">
							<figure>
								<a href="javascript:void(0)">
									<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
								</a>
							</figure>
							<div class="info">
								<div class="title">
									<a href="javascript:void(0)">
										<h3>
											<?php echo $post_title; ?>
										</h3>
									</a>
								</div>
								<div class="meta">
									<div class="price">
			                            <span class="price-news">
			                            	<?php echo show_price_old_price($oldprice,$price,$donvi); ?>
			                            </span>
			                        </div>
									<div class="more">
										<i class="fa fa-eye" aria-hidden="true"></i> 
										<?php echo $video_view; ?> lượt xem
									</div>
								</div>
							</div>
						</div>
					</article>

				<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

				<!-- // -->
				<?php
					$i = 0;
					$query = query_post_by_taxonomy_paged($glb_ctp_video, $taxonomy_name, $term_id, -1);
					if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
				?>
				<?php $i++; endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

				<?php if( $i > 9 ) { ?>
					<div class="more-post">
						<a href="javascript:void(0)" class="btn smore-btn" data-catid='<?php echo $term_id; ?>'>
							Xem thêm <i class="fa fa-sort-desc" aria-hidden="true"></i>
						</a>
					</div>
				<?php } ?>

			</div>
        </div>
    </div>
	<!-- Modal -->
	<div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<iframe width="560" height="315" src="" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>