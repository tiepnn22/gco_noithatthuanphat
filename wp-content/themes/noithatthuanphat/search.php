<?php get_header(); ?>

<?php get_template_part("resources/views/page-banner",$data_page_banner); ?>

<section class="page-product">
    <div class="container">
        <div class="page-product-content">
        	
        	<h2>
        		Sản phẩm
    		</h2>
    		<div class="row">

				<?php
					$query = query_search_post_paged( $_GET['s'], array('product'), -1 );
					$max_num_pages = $query->max_num_pages;
					
					if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

					$post_id 		= get_the_ID();
            		$post_title 	= get_the_title($post_id);
                    $post_content 	= wpautop(get_the_content($post_id));
            		$post_date 		= get_the_date('Y/m/d', $post_id);
            		$post_link 		= get_post_permalink($post_id);
            		$post_image 	= getPostImage($post_id,"p-product");
            		$post_excerpt 	= cut_string(get_the_excerpt($post_id),80,'...');
            		$post_author 	= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
            		$post_tag 		= get_the_tags($post_id);
				?>

					<article class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12">
						<div class="item">
							<figure>
								<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
									<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
								</a>
							</figure>
							<div class="info">
								<div class="title">
									<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
										<h3>
											<?php echo $post_title; ?>
										</h3>
									</a>
								</div>
								<?php
									//show_price
									$data_show_price = array(
										'post_id'     =>    $post_id
									);
									get_template_part("resources/views/wc-product-price",$data_show_price);
								?>
							</div>
						</div>
					</article>

				<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

            </div>
            
		</div>
	</div>
</section>

<section class="page-news">
    <div class="container">
        <div class="page-news-content">

        	<h2>
        		Tin tức
    		</h2>
    		<div class="row">

				<?php
					$query = query_search_post_paged( $_GET['s'], array('post'), -1 );
					$max_num_pages = $query->max_num_pages;
					
					if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

					$post_id 		= get_the_ID();
            		$post_title 	= get_the_title($post_id);
                    $post_content 	= wpautop(get_the_content($post_id));
            		$post_date 		= get_the_date('Y/m/d', $post_id);
            		$post_link 		= get_post_permalink($post_id);
            		$post_image 	= getPostImage($post_id,"news");
            		$post_excerpt 	= cut_string(get_the_excerpt($post_id),60,'...');
            		$post_author 	= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
            		$post_tag 		= get_the_tags($post_id);
				?>

					<article class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
						<div class="item">
							<figure>
								<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
									<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
								</a>
							</figure>
							<div class="info">
								<div class="title">
									<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
										<h3>
											<?php echo $post_title; ?>
										</h3>
									</a>
								</div>
								<div class="desc">
					                <?php echo $post_excerpt; ?>
								</div>
								<div class="read-more">
									<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">Chi tiết</a>
								</div>
							</div>
						</div>
					</article>

                <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

            </div>

        </div>
    </div>
</section>

<?php get_footer(); ?>