<?php get_header(); ?>
	
<?php
	//field
	$home_slide = get_field('home_slide');

	$home_service = get_field('home_service');

	$home_product_feature_select_cat_title_section = get_field('home_product_feature_select_cat_title_section');
	$home_product_feature_select_cat = get_field('home_product_feature_select_cat');

	$home_product_feature_select_title_section = get_field('home_product_feature_select_title_section');
	$home_product_feature_select = get_field('home_product_feature_select');

	$home_product_handing_select_title_section = get_field('home_product_handing_select_title_section');
	$home_product_handing_select = get_field('home_product_handing_select');

	$home_video_feature_select_title_section = get_field('home_video_feature_select_title_section');
	$home_video_feature_select = get_field('home_video_feature_select');

	$home_product_tab_select_cat_title_section = get_field('home_product_tab_select_cat_title_section');
	$home_product_tab_select_cat = get_field('home_product_tab_select_cat');

	$home_image_ads = get_field('home_image_ads');
	$home_image_ads_url = get_field('home_image_ads_url');

	$home_video_select_title_section = get_field('home_video_select_title_section');
	$home_video_select_link = get_field('home_video_select_link');
	$home_video_select = get_field('home_video_select');

	$home_product_thematic_select_title_section = get_field('home_product_thematic_select_title_section');
	$home_product_thematic_select = get_field('home_product_thematic_select');

	$home_product_deco_select_title_section = get_field('home_product_deco_select_title_section');
	$home_product_deco_select = get_field('home_product_deco_select');

	$home_news_select_title_section = get_field('home_news_select_title_section');
	$home_news_select = get_field('home_news_select');
?>

<h1 style="display: none;">Nội thất Thuận Phát</h1>

<?php if(!empty( $home_slide )) { ?>
<section class="slider home-slider">
	<div class="slider-content">

        <?php
            foreach ($home_slide as $foreach_kq) {

            $gallery_image = $foreach_kq;
        ?>

            <img src="<?php echo $gallery_image; ?>" alt="slider">

        <?php } ?>

	</div>
</section>
<?php } ?>

<?php if(!empty( $home_service )) { ?>
<section class="home-service">
	<div class="container">
		<div class="home-service-content">
			<div class="row">

				<?php
				    foreach ($home_service as $foreach_kq) {

				    $post_image = $foreach_kq["image"];
				    $post_title = $foreach_kq["title"];
				    $post_desc  = $foreach_kq["desc"];
				?>

					<article class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
						<div class="item">
							<figure><img src="<?php echo $post_image; ?>"></figure>
							<div class="info">
								<div class="title"><?php echo $post_title; ?></div>
								<div class="desc"><?php echo $post_desc; ?></div>
							</div>
						</div>
					</article>
				
				<?php } ?>

			</div>
		</div>
	</div>
</section>
<?php } ?>

<?php if(!empty( $home_product_feature_select_cat )) { ?>
<section class="home-product-feature">
	<div class="title-section">
		<h2><?php echo $home_product_feature_select_cat_title_section; ?></h2>
	</div>
	<div class="home-product-feature-content">
		<div class="row">

			<?php
				foreach ($home_product_feature_select_cat as $foreach_kq) {

				$term_id = $foreach_kq->term_id;
				$term_desc = cut_string( $foreach_kq->description ,250,'...');
				$taxonomy_slug = $foreach_kq->taxonomy;
        		$term_name = get_term( $term_id, $taxonomy_slug )->name;
        		$term_link = get_term_link(get_term( $term_id ));

				$page_banner_check = get_field('page_banner', 'category_'.$term_id);
				$page_banner = (!empty($page_banner_check)) ? $page_banner_check : '';

				$page_banner_icon_check = get_field('page_banner_icon', 'category_'.$term_id);
				$page_banner_icon = (!empty($page_banner_icon_check)) ? $page_banner_icon_check : '';
			?>

				<article class="col-xl-4 col-lg-6 col-md-6 col-sm-6 col-12">
					<div class="item">
						<figure>
							<a href="javascript:void(0)" title="<?php echo $term_name; ?>">
								<img src="<?php echo $page_banner; ?>" alt="<?php echo $term_name; ?>">
							</a>
						</figure>
						<div class="info">
							<div class="icon">
				                <img src="<?php echo $page_banner_icon; ?>">
							</div>
							<div class="desc">
								<?php echo $term_desc; ?>
							</div>
							<div class="read-more">
								<a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>">
									Chi tiết <i class="fa fa-angle-right" aria-hidden="true"></i>
								</a>
							</div>
						</div>
					</div>
					<div class="title">
						<a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>">
							<h3>
								<?php echo $term_name; ?>
							</h3>
						</a>
					</div>
				</article>

			<?php } ?>

		</div>
	</div>
</section>
<?php } ?>

<section class="home-product-intro">
	<div class="container">
		<div class="home-product-intro-content">
			<div class="row">

				<?php if(!empty( $home_product_feature_select )) { ?>
				<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 home-product-sale">
					<div class="title-section-col">
						<h2><?php echo $home_product_feature_select_title_section; ?></h2>
					</div>
					<div class="home-product-sale-content">
						<div class="row">

							<?php
							    foreach ($home_product_feature_select as $foreach_kq) {

							    $post_id 			= $foreach_kq->ID;
							    $post_title 		= get_the_title($post_id);
							    $post_date 			= get_the_date('Y/m/d',$post_id);
							    $post_link 			= get_post_permalink($post_id);
							    $post_image 		= getPostImage($post_id,"p-product");
							    $post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
							    $post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
							    $post_tag 			= get_the_tags($post_id);
							?>

								<article class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
									<div class="item">
										<figure>
											<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
												<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
											</a>
										</figure>
										<div class="info">
											<div class="title">
												<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
													<h3>
														<?php echo $post_title; ?>
													</h3>
												</a>
											</div>
											<?php
												//show_price
												$data_show_price = array(
													'post_id'     =>    $post_id
												);
												get_template_part("resources/views/wc-product-price",$data_show_price);
											?>
										</div>
									</div>
								</article>

							<?php } ?>

						</div>
					</div>
				</div>
				<?php } ?>

				<?php if(!empty( $home_product_handing_select )) { ?>
				<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 home-product-ship">
					<div class="title-section-col">
						<h2><?php echo $home_product_handing_select_title_section; ?></h2>
					</div>
					<div class="home-product-ship-content">

						<?php
						    foreach ($home_product_handing_select as $foreach_kq) {

						    $post_id 			= $foreach_kq->ID;
						    $post_title 		= get_the_title($post_id);
						    $post_date 			= get_the_date('Y/m/d',$post_id);
						    $post_link 			= get_post_permalink($post_id);
						    $post_image 		= getPostImage($post_id,"p-product");
						    $post_excerpt 		= cut_string(get_the_excerpt($post_id),250,'...');
						    $post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
						    $post_tag 			= get_the_tags($post_id);
						?>

							<article class="item">
								<figure>
									<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
										<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
									</a>
								</figure>
								<div class="info">
									<div class="title">
										<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
											<h3>
												<?php echo $post_title; ?>
											</h3>
										</a>
									</div>
									<div class="desc">
										<?php echo $post_excerpt; ?>
			                        </div>
								</div>
							</article>

						<?php } ?>

					</div>
				</div>
				<?php } ?>

			</div>
		</div>
	</div>
</section>

<?php if(!empty( $home_video_feature_select )) { ?>
<section class="home-video-product-feature">
	<div class="container">
		<div class="title-section">
			<h2><?php echo $home_video_feature_select_title_section; ?></h2>
		</div>
		<div class="home-video-product-feature-content">
			<div class="row">

				<?php
				    foreach ($home_video_feature_select as $foreach_kq) {

				    $post_id 		= $foreach_kq->ID;
                    $post_title 	= get_the_title($post_id);
                    $post_content 	= wpautop(get_the_content($post_id));
                    $post_date 		= get_the_date('Y/m/d',$post_id);
                    $post_link 		= get_post_permalink($post_id);
                    $post_image 	= getPostImage($post_id,"p-product");
                    $post_excerpt 	= cut_string(get_the_excerpt($post_id),80,'...');
                    $post_author 	= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
                    $post_tag 		= get_the_tags($post_id);

					$video_url 		= getIframeVideo( get_field('video_url', $post_id) );
					$oldprice 		= get_field('video_price_regular', $post_id);
					$donvi 			= " đ";
					$price 			= '';
					$video_view_check 	= get_field('video_view', $post_id);
					$video_view 	= (!empty($video_view_check)) ? $video_view_check : '0';
				?>

					<article class="col-4">
						<div class="item" data-toggle="modal" data-target="#Modal1" data-video="<?php echo $video_url; ?>">
							<figure>
								<a href="javascript:void(0)" title="<?php echo $post_title; ?>">
									<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
								</a>
							</figure>
							<div class="info">
								<div class="title">
									<a href="javascript:void(0)" title="<?php echo $post_title; ?>">
										<h3>
											<?php echo $post_title; ?>
										</h3>
									</a>
								</div>
								<div class="meta">
									<div class="price">
			                            <span class="price-news">
			                            	<?php echo show_price_old_price($oldprice,$price,$donvi); ?>
			                            </span>
			                        </div>
									<div class="more">
										<i class="fa fa-eye" aria-hidden="true"></i> 
										<?php echo $video_view; ?> lượt xem
									</div>
								</div>
							</div>
						</div>
					</article>

				<?php } ?>

			</div>
		</div>
	</div>
</section>
<?php } ?>

<?php
	$i = 0;
	foreach ($home_product_tab_select_cat as $foreach_kq) {

	$term_id = $foreach_kq->term_id;
	$term_desc = cut_string( $foreach_kq->description ,300,'...');
	$taxonomy_slug = $foreach_kq->taxonomy;
	$term_name = get_term( $term_id, $taxonomy_slug )->name;
	$term_link = get_term_link(get_term( $term_id ));

	// child cat
	$term_childs = get_term_children( $term_id, $taxonomy_slug );
	$count = count($term_childs);
	
	// if($count > 0) {
?>

		<section class="home-product-tab">
			<div class="container">
				<div class="home-product-tab-content">
					<div class="product-tab-top <?php if($count == 0) { echo 'product-tab-top-mobile'; } ?>">
						
						<div class="product-tab-title">
							<h2><?php echo $term_name; ?></h2>
						</div>
			            <ul class="nav nav-tabs" role="tablist">

							<?php
								$j = 1;
								foreach ($term_childs as $foreach_kq) {
								
								$term_childs_id = $foreach_kq;
								$term_childs_name = get_term( $term_childs_id, $taxonomy_slug )->name;
							?>
						            <li class="nav-item">
						                <a class="nav-link <?php if($j == 1){ echo 'active'; } ?>" href="#profile_<?php echo $i; ?>_<?php echo $j; ?>" role="tab" data-toggle="tab">
						                    <?php echo $term_childs_name; ?>
						                </a>
						            </li>
							<?php
								$j++; }
							?>

			            </ul>
			            <a class="product-tab-link" href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>">
			            	<i class="fa fa-chevron-right" aria-hidden="true"></i>
			            </a>

			        </div>

		            <div class="tab-content">

		            	<!--Nếu ko có cat con-->
						<?php if($count == 0) { ?>
						    <div role="tabpanel" class="tab-pane active" id="">
						        <div class="row">

									<?php
										$query = query_post_by_taxonomy_paged('product', $taxonomy_slug, $term_id, 8);

										if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

						                $post_id 		= get_the_ID();
						                $post_title 	= get_the_title($post_id);
						                $post_content 	= wpautop(get_the_content($post_id));
						                $post_date 		= get_the_date('Y/m/d',$post_id);
						                $post_link 		= get_post_permalink($post_id);
						                $post_image 	= getPostImage($post_id,"p-product");
						                $post_excerpt 	= cut_string(get_the_excerpt($post_id),80,'...');
						                $post_author 	= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
						                $post_tag 		= get_the_tags($post_id);
									?>

										<article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
											<div class="item">
												<figure>
													<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
														<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
													</a>
												</figure>
												<div class="info">
													<div class="title">
														<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
															<h3>
																<?php echo $post_title; ?>
															</h3>
														</a>
													</div>
													<?php
														//show_price
														$data_show_price = array(
															'post_id'     =>    $post_id
														);
														get_template_part("resources/views/wc-product-price",$data_show_price);
													?>
												</div>
											</div>
										</article>
										
									<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

								</div>
						    </div>
						<?php } ?>

						<!--Nếu có cat con-->
						<?php
							$j = 1;
							foreach ($term_childs as $foreach_kq) {
							
							$term_childs_id = $foreach_kq;
							$term_childs_name = get_term( $term_childs_id, $taxonomy_slug )->name;
						?>

				                <div role="tabpanel" class="tab-pane <?php if($j == 1){ echo 'active'; } ?>" id="profile_<?php echo $i; ?>_<?php echo $j; ?>">
						            <div class="row">

										<?php
											$query = query_post_by_taxonomy_paged('product', $taxonomy_slug, $term_childs_id, 8);

											if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

					                        $post_id 		= get_the_ID();
					                        $post_title 	= get_the_title($post_id);
					                        $post_content 	= wpautop(get_the_content($post_id));
					                        $post_date 		= get_the_date('Y/m/d',$post_id);
					                        $post_link 		= get_post_permalink($post_id);
					                        $post_image 	= getPostImage($post_id,"p-product");
					                        $post_excerpt 	= cut_string(get_the_excerpt($post_id),80,'...');
					                        $post_author 	= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
					                        $post_tag 		= get_the_tags($post_id);
										?>

											<article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
												<div class="item">
													<figure>
														<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
															<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
														</a>
													</figure>
													<div class="info">
														<div class="title">
															<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
																<h3>
																	<?php echo $post_title; ?>
																</h3>
															</a>
														</div>
														<?php
															//show_price
															$data_show_price = array(
																'post_id'     =>    $post_id
															);
															get_template_part("resources/views/wc-product-price",$data_show_price);
														?>
													</div>
												</div>
											</article>
											
										<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

									</div>
				                </div>

						<?php
							$j++; }
						?>

		            </div>

				</div>
			</div>
		</section>

		<?php
			if($i == 0) {
				if(!empty( $home_image_ads )) {
		?>
			<section class="home-image-ads">
				<div class="container">
					<a href="<?php echo $home_image_ads_url; ?>" target="_blank">
						<img src="<?php echo $home_image_ads; ?>">
					</a>
				</div>
			</section>
		<?php
				}
			}
		?>

<?php
	// }
	$i++; }
?>

<?php if(!empty( $home_video_select )) { ?>
<section class="home-product-tab home-product-tab-video">
	<div class="container">
		<div class="home-product-tab-content">
			<div class="product-tab-top product-tab-top-mobile">
				
				<div class="product-tab-title">
					<h2><?php echo $home_video_select_title_section; ?></h2>
				</div>
	            <a class="product-tab-link" href="<?php echo $home_video_select_link; ?>">
	            	<i class="fa fa-chevron-right" aria-hidden="true"></i>
	            </a>

	        </div>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="profile_">
		            <div class="row">

						<?php
						    foreach ($home_video_select as $foreach_kq) {

						    $post_id 		= $foreach_kq->ID;
		                    $post_title 	= get_the_title($post_id);
		                    $post_content 	= wpautop(get_the_content($post_id));
		                    $post_date 		= get_the_date('Y/m/d',$post_id);
		                    $post_link 		= get_post_permalink($post_id);
		                    $post_image 	= getPostImage($post_id,"p-product");
		                    $post_excerpt 	= cut_string(get_the_excerpt($post_id),80,'...');
		                    $post_author 	= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
		                    $post_tag 		= get_the_tags($post_id);

							$video_url 		= getIframeVideo( get_field('video_url', $post_id) );
							$oldprice 		= get_field('video_price_regular', $post_id);
							$donvi 			= " đ";
							$price 			= '';
						?>

							<article class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
								<div class="item" data-toggle="modal" data-target="#Modal1" data-video="<?php echo $video_url; ?>">
									<figure>
										<a href="javascript:void(0)" title="<?php echo $post_title; ?>">
											<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
										</a>
									</figure>
									<div class="info">
										<div class="title">
											<a href="javascript:void(0)" title="<?php echo $post_title; ?>">
												<h3>
													<?php echo $post_title; ?>
												</h3>
											</a>
										</div>
										<?php echo show_price_old_price($oldprice,$price,$donvi); ?>
									</div>
								</div>
							</article>

						<?php } ?>

					</div>
                </div>
            </div>

		</div>
	</div>
</section>
<?php } ?>

<section class="home-news-intro">
	<div class="container">
		<div class="home-news-intro-content">
			<div class="row">

				<?php if(!empty( $home_product_thematic_select )) { ?>
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 home-news-subject">
					<div class="title-section-col">
						<h2><?php echo $home_product_thematic_select_title_section; ?></h2>
					</div>
					<div class="home-news-subject-content">

						<?php
						    foreach ($home_product_thematic_select as $foreach_kq) {

						    $post_id 			= $foreach_kq->ID;
						    $post_title 		= cut_string(get_the_title($post_id),40,'...');
						    $post_date 			= get_the_date('d/m/Y',$post_id);
						    $post_link 			= get_post_permalink($post_id);
						    $post_image 		= getPostImage($post_id,"p-post");
						    $post_excerpt 		= cut_string(get_the_excerpt($post_id),60,'...');
						    $post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
						    $post_tag 			= get_the_tags($post_id);
						?>

							<article class="item">
								<figure>
									<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
										<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
									</a>
								</figure>
								<div class="info">
									<div class="title">
										<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
											<h3>
												<?php echo $post_title; ?>
											</h3>
										</a>
									</div>
									<div class="desc">
						                <?php echo $post_excerpt; ?>
									</div>
									<div class="read-more">
										<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">Chi tiết</a>
									</div>
								</div>
							</article>	

						<?php } ?>

					</div>
				</div>
				<?php } ?>

				<?php if(!empty( $home_product_deco_select )) { ?>
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 home-news-deco">
					<div class="title-section-col">
						<h2><?php echo $home_product_deco_select_title_section; ?></h2>
					</div>
					<div class="home-news-deco-content">

						<?php
						    foreach ($home_product_deco_select as $foreach_kq) {

						    $post_id 			= $foreach_kq->ID;
						    $post_title 		= cut_string(get_the_title($post_id),40,'...');
						    $post_date 			= get_the_date('d/m/Y',$post_id);
						    $post_link 			= get_post_permalink($post_id);
						    $post_image 		= getPostImage($post_id,"p-post");
						    $post_excerpt 		= cut_string(get_the_excerpt($post_id),60,'...');
						    $post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
						    $post_tag 			= get_the_tags($post_id);
						?>

							<article class="item">
								<figure>
									<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
										<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
									</a>
								</figure>
								<div class="info">
									<div class="title">
										<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
											<h3>
												<?php echo $post_title; ?>
											</h3>
										</a>
									</div>
									<div class="desc">
						                <?php echo $post_excerpt; ?>
									</div>
									<div class="read-more">
										<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">Chi tiết</a>
									</div>
								</div>
							</article>	

						<?php } ?>

					</div>
				</div>
				<?php } ?>

				<?php if(!empty( $home_news_select )) { ?>
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 home-news-new">
					<div class="title-section-col">
						<h2><?php echo $home_news_select_title_section; ?></h2>
					</div>
					<div class="home-news-new-content">

						<?php
							$i = 0;
						    foreach ($home_news_select as $foreach_kq) {

						    $post_id 			= $foreach_kq->ID;
						    $post_title 		= get_the_title($post_id);
						    $post_date 			= get_the_date('d/m/Y',$post_id);
						    $post_link 			= get_post_permalink($post_id);
						    $post_image 		= getPostImage($post_id,"p-post");
						    $post_excerpt 		= cut_string(get_the_excerpt($post_id),80,'...');
						    $post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
						    $post_tag 			= get_the_tags($post_id);

						    if($i == 0) {
						?>

								<article class="item">
									<figure>
										<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
											<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
										</a>
									</figure>
									<div class="info">
										<div class="title">
											<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
												<h3>
													<?php echo $post_title; ?>
												</h3>
											</a>
										</div>
										<div class="desc">
											<?php echo $post_excerpt; ?>
				                        </div>
									</div>
								</article>

						<?php } else { ?>

								<a class="item-title" href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
									<i class="fa fa-angle-double-right" aria-hidden="true"></i>
									<?php echo $post_title; ?>
								</a>

						<?php } $i++; } ?>

					</div>
				</div>
				<?php } ?>

			</div>
		</div>
	</div>
</section>



<?php get_footer(); ?>


<!-- Modal -->
<div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

			<iframe width="560" height="315" src="" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

		</div>
	</div>
</div>
