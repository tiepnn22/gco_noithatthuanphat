<?php get_header(); ?>

<?php
	$page_id       = get_the_ID();
	$page_name     = get_the_title();
	$page_content  = get_the_content();

	//banner
    $page_banner_check = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'full');
    $page_banner = (!empty($page_banner_check[0])) ? $page_banner_check[0] : '';
    if( !empty($page_banner) ) {
        $data_page_banner = array(
            'image_link'     =>    $page_banner, 
            'image_alt'    =>    $page_name
        );
    }
?>

<?php get_template_part("resources/views/page-banner",$data_page_banner); ?>

<section class="page-intro">
    <div class="container">

        <div class="page-title">
            <h1><?php echo $page_name; ?></h1>
        </div>
        
        <div class="page-intro-content">
            <?php echo $page_content; ?>
        </div>

    </div>
</section>

<?php get_footer(); ?>