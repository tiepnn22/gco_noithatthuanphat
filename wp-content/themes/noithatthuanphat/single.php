<?php get_header(); ?>

<?php
	$post_id = get_the_ID();

	$get_category = get_the_category($post_id);
	foreach ( $get_category as $get_category_kq ) {
	    $cat_id = $get_category_kq->term_id;
	}
	$cat_name = get_cat_name($cat_id);
	
	//info post
	$single_post_title 		= get_the_title($post_id);
	$single_post_content 	= wpautop(get_the_content($post_id));
	$single_post_date 		= get_the_date('d/m/Y', $post_id);
	$single_post_link 		= get_post_permalink($post_id);
    $single_post_image 		= getPostImage($post_id,"p-post");
	$single_post_excerpt 	= cut_string(get_the_excerpt($post_id),300,'...');
	$single_recent_author 	= get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
	$single_post_author 	= $single_recent_author->display_name;
    $single_post_tag 		= get_the_tags($post_id);

	//banner
	$page_banner_check = get_field('page_banner', 'category_'.$cat_id);
	$page_banner = (!empty($page_banner_check)) ? $page_banner_check : '';
	$data_page_banner = array(
		'image_link'     =>    $page_banner, 
		'image_alt'    =>    $cat_name
	);
?>

<?php get_template_part("resources/views/page-banner",$data_page_banner); ?>

<section class="single-news">
    <div class="container">
    	<div class="row">

	    	<div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 col-12 single-news-content">
				<div class="single-title">
					<h1><?php echo $single_post_title; ?></h1>
				</div>
				<div class="single-date">
					<i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $single_post_date; ?>
				</div>
				<div class="single-content">
		        	<?php echo $single_post_content; ?>
		        </div>
		        <?php get_template_part("resources/views/social-bar"); ?>
	    	</div>

	    	<?php get_sidebar();?>

    	</div>
    </div>
</section>

<?php get_template_part("resources/views/template-related-post"); ?>

<?php get_footer(); ?>