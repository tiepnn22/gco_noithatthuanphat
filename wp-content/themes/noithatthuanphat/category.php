<?php get_header(); ?>

<?php
	$category_info 	= get_category_by_slug( get_query_var( 'category_name' ) );
	$cat_id 		= $category_info->term_id;
	$cat_name 		= get_cat_name($cat_id);
	$cat_excerpt 	= wpautop(category_description($cat_id));
	$cat_link 		= esc_url(get_term_link($cat_id));
	
	//banner
	$page_banner_check = get_field('page_banner', 'category_'.$cat_id);
	$page_banner = (!empty($page_banner_check)) ? $page_banner_check : '';
	$data_page_banner = array(
		'image_link'     =>    $page_banner, 
		'image_alt'    =>    $cat_name
	);
?>

<?php get_template_part("resources/views/page-banner",$data_page_banner); ?>

<section class="page-news">
    <div class="container">
    	<div class="row">

    		<?php get_sidebar();?>

	    	<div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 col-12 page-news-content">
	    		<div class="row">

					<?php
						$query = query_post_by_category_paged($cat_id, 12);
						$max_num_pages = $query->max_num_pages;

						if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

                        $post_id = get_the_ID();
                        $post_title = cut_string(get_the_title($post_id),60,'...');
                        $post_content = wpautop(get_the_content($post_id));
                        $post_date = get_the_date('Y/m/d',$post_id);
                        $post_link = get_post_permalink($post_id);
                        $post_image = getPostImage($post_id,"p-post");
                        $post_excerpt = cut_string(get_the_excerpt($post_id),80,'...');
                        $post_author = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
                        $post_tag = get_the_tags($post_id);
					?>

						<article class="col-xl-6 col-lg-6 col-md-12 col-sm-6 col-12">
							<div class="item">
								<figure>
									<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
										<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
									</a>
								</figure>
								<div class="info">
									<div class="title">
										<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
											<h3>
												<?php echo $post_title; ?>
											</h3>
										</a>
									</div>
									<div class="desc">
						                <?php echo $post_excerpt; ?>
									</div>
									<div class="read-more">
										<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">Chi tiết</a>
									</div>
								</div>
							</div>
						</article>

					<?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

				</div>
	
				<nav class="navigation">
					<?php echo paginationCustom( $max_num_pages ); ?>
				</nav>
	    	</div>

    	</div>
    </div>
</section>

<?php get_footer(); ?>