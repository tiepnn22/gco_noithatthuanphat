<?php
	//$glb_ctp_product biến toàn cục
	//$glb_tax_product biến toàn cục
    global $glb_ctp_product;
    global $glb_tax_product;

	global $post;
	$terms = get_the_terms( $post->ID , $glb_tax_product, 'string');
	$term_ids = wp_list_pluck($terms,'term_id');
	$query = new WP_Query( array(
		'post_type' => $glb_ctp_product,
		'tax_query' => array(
			array(
				'taxonomy' => $glb_tax_product,
				'field' => 'id',
				'terms' => $term_ids,
				'operator'=> 'IN'
			 )),
		'posts_per_page' => 5,
		'orderby' => 'date',
		'post__not_in'=>array($post->ID)
	) );
?>

<aside class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 sidebar sidebar-single-product">
	<section class="widget widget-list-post">

		<?php if($query->have_posts()) { ?>

			<h2 class="title-widget"><?php _e('Sản phẩm liên quan', 'text_domain'); ?></h2>
			<div class="widget-list-post-content">

				<?php
					while ($query->have_posts() ) : $query->the_post();

	                $post_id = get_the_ID();
	                $post_title = get_the_title($post_id);
	                $post_content = wpautop(get_the_content($post_id));
	                $post_date = get_the_date('Y/m/d',$post_id);
	                $post_link = get_post_permalink($post_id);
	                $post_image = getPostImage($post_id,"p-post");
	                $post_excerpt = cut_string(get_the_excerpt($post_id),500,'...');
	                $post_tag = get_the_tags($post_id);

	            ?>

					<div class="item">
						<figure>
							<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
								<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
							</a>
						</figure>
						<div class="info">
							<div class="title">
								<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
									<h3>
										<?php echo $post_title; ?>
									</h3>
								</a>
							</div>
							<?php
								//show_price
								$donvi = " đ";

									$oldprice = get_field('product_price_regular', $post_id);
									$price = get_field('product_price_sale', $post_id);

								echo show_price_old_price($oldprice,$price,$donvi);
							?>
						</div>
					</div>

	            <?php endwhile; wp_reset_postdata(); ?>
				
			</div>
			
		<?php } else { echo ''; } ?>

	</section>
</aside>
