<?php
	global $data_page_banner;
	
	if(!empty( $data_page_banner )) {
		$image_link = $data_page_banner['image_link'];
		$image_alt = $data_page_banner['image_alt'];
	}
?>

<?php if(!empty( $image_link )) { ?>

	<section class="page-banner">
		<img src="<?php echo $image_link;?>" alt="<?php echo $image_alt;?>">
	</section>

<?php } ?>

<section class="breadcrumbs">
	<div class="container">

	    <?php
	        if(function_exists('bcn_display')) { 
	            echo '<a href="' . site_url() . '">Trang chủ </a> &nbsp; >';
	            bcn_display();
	        }
	    ?>

	</div>
</section>