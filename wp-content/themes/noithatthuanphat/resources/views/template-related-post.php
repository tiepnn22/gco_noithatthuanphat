<?php
    global $post;
    $categories = get_the_category($post->ID);

    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    $args=array(
	    'category__in' => $category_ids,
	    'post__not_in' => array($post->ID),
	    'posts_per_page'=> 4,
	    'ignore_sticky_posts'=>1
    );
    $query = new wp_query( $args );
?>

<aside class="related-post">
    <div class="container">
        <div class="related-title">
            <h3>
                <?php _e('Tin cùng chuyên mục', 'text_domain'); ?>
            </h3>
        </div>
        <div class="related-post-content">
            <div class="row">

                <?php
                    if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

                    $post_id = get_the_ID();
                    $post_title = get_the_title($post_id);
                    $post_content = wpautop(get_the_content($post_id));
                    $post_date = get_the_date('d/m/Y',$post_id);
                    $post_link = get_post_permalink($post_id);
                    $post_image = getPostImage($post_id,"p-post");
                    $post_excerpt = cut_string(get_the_excerpt($post_id),100,'...');
                    $post_author = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
                    $post_tag = get_the_tags($post_id);
                ?>

                <article class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                    <div class="item">
                        <figure>
                            <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                                <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                            </a>
                        </figure>
                        <div class="info">
                            <div class="title">
                                <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" rel="bookmark">
                                    <h4>
                                        <?php echo $post_title; ?>
                                    </h4>
                                </a>
                            </div>
                            <div class="date">
                                <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $post_date; ?>
                            </div>
                            <div class="desc">
                                <?php echo $post_excerpt; ?>
                            </div>
                        </div>
                    </div>
                </article>

                <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

            </div>
        </div>
    </div>
</aside>