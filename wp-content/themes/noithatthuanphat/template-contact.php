<?php
	/*
	Template Name: Mẫu Liên hệ
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //banner
    $page_banner_check  = wp_get_attachment_image_src(get_post_thumbnail_id($page_id), 'full');
    $page_banner        = (!empty($page_banner_check[0])) ? $page_banner_check[0] : '';
    $data_page_banner = array(
        'image_link'     =>    $page_banner, 
        'image_alt'      =>    $page_name
    );
    
    //field
    $contact_info_title = get_field('contact_info_title');
    $contact_info_address = get_field('contact_info_address');

    $contact_contact_title = get_field('contact_contact_title');
    $contact_contact_form = get_field('contact_contact_form');

    $contact_map = get_field('contact_map');
?>

<?php get_template_part("resources/views/page-banner",$data_page_banner); ?>

<section class="page-contact">
    <div class="container">

        <div class="page-contact-content">
            <div class="row">
                
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="contact-title">
                        <h2><?php echo $contact_info_title; ?></h2>
                    </div>
                    <div class="contact-info">

                        <?php
                            foreach ($contact_info_address as $contact_info_address_kq) {

                            $post_title = $contact_info_address_kq["title"];
                            $post_address = $contact_info_address_kq["address"];
                            $post_phone = $contact_info_address_kq["phone"];
                            $post_email = $contact_info_address_kq["email"];
                        ?>

                            <address>
                                <div class="contact-info-title"><?php echo $post_title; ?></div>
                                <p>
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                    <span><?php echo $post_address; ?></span>
                                </p>
                            </address>
                            <p>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <span><?php echo $post_phone; ?></span>
                            </p>
                            <p>
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <span><?php echo $post_email; ?></span>
                            </p>

                        <?php } ?>

                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="contact-title">
                        <h2><?php echo $contact_contact_title; ?></h2>
                    </div>
                    
                    <?php if(!empty( $contact_contact_form )) { ?>
                    <div class="contact-form">
                        <?php echo do_shortcode($contact_contact_form); ?>
                    </div>
                    <?php } ?>
                </div>

            </div>
        </div>

        <map class="contact-map">
            <img src="<?php echo asset('images/2x1.png'); ?>">
            <?php echo $contact_map; ?>
        </map>

    </div>
</section>

<?php
    if(!empty( $contact_contact_form )) {
        // Call funtion contact form
        if ( function_exists( 'wpcf7_enqueue_scripts' ) ) { wpcf7_enqueue_scripts(); }
        if ( function_exists( 'wpcf7_enqueue_styles' ) ) { wpcf7_enqueue_styles(); }
    }
?>

<?php get_footer(); ?>