<?php
    //field
    $sidebar_form_title = get_field('sidebar_form_title', 'option');
    $sidebar_form = get_field('sidebar_form', 'option');

    $sidebar_select_post_title = get_field('sidebar_select_post_title', 'option');
    $sidebar_select_post = get_field('sidebar_select_post', 'option');

    $sidebar_image_ads = get_field('sidebar_image_ads', 'option');

    $sidebar_single_select_post_title = get_field('sidebar_single_select_post_title', 'option');
    $sidebar_single_select_post = get_field('sidebar_single_select_post', 'option');

    $sidebar_single_select_product_title = get_field('sidebar_single_select_product_title', 'option');
    $sidebar_single_select_product = get_field('sidebar_single_select_product', 'option');
?>

<?php
	if ( is_category() || is_tax() ) {
?>
		<aside class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 sidebar sidebar-news">

			<?php if ( is_category() ) { ?>
			<!-- <section class="widget widget-list-cat">
				<h2 class="title-widget">Danh mục</h2>
				<div class="widget-list-cat-content">
					<div class="menu-click-show">
						<ul>
							<?php
								$categories = get_categories( array(
								    'orderby' => 'name',
								    'parent'  => 0
								) );

								foreach ( $categories as $category ) {
									if($category->term_id == 1) {} else {
										echo '<li><a href="'.esc_url(get_term_link($category->term_id)).'">'.$category->name.'</a>';
									}

									    // cap 2
									    $team_lg = $category->term_id;
									    $child_cats = get_term_children( $category->term_id, 'category' );

										$count = count($child_cats);
										if($count > 0) {
											echo '<ul class="sub-menu">';

										    foreach ($child_cats as $childs) {
										        $child_catss = get_term_by( 'id', $childs, 'category' );

										        // if($child_catss->parent == $team_lg) {
										        	echo '<li><a href="'.esc_url(get_term_link($child_catss->term_id)).'">&nbsp;&nbsp;&nbsp;'.$child_catss->name.'</a>';

														// cap 3
														// $term_childs = get_term_children( $child_catss->term_id, 'category' );

														// $count = count($term_childs);
														// if($count > 0) {
														// 	echo '<ul class="sub-menu">';

														// 	foreach ( $term_childs as $childss ) {
														// 		$child_catsss = get_term_by( 'id', $childss, 'category' );

														// 		echo '<li><a href="'.esc_url(get_term_link($child_catsss->term_id)).'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child_catsss->name.'</a></li>';
														// 	}
														// 	echo '</ul>';
														// }
													echo '</li>';
										        // }
										    }
									    	echo '</ul>';
										}
								    echo '</li>';
								}
							?>
						</ul>
					</div>
				</div>
			</section> -->
			<?php } ?>

			<!-- <?php if ( is_tax() ) { ?> -->
    		<!-- <?php } ?> -->
    		<section class="widget widget-list-cat">
    			<h2 class="title-widget">Danh mục</h2>
    			<div class="widget-list-cat-content">
					<div class="menu-click-show">
						<ul>
							<?php
								$taxonomy_name = 'product-cat';
								$terms = get_terms('product-cat', array(
								    'parent'=> 0,
								    'hide_empty' => false
								) );
								foreach($terms as $term){
									echo '<li><a href="'.esc_url(get_term_link($term->term_id)).'">'.$term->name.'</a>';

										// cap 2
										$team_lg = $term->term_id;
										$term_childs = get_term_children( $term->term_id, $taxonomy_name );

										$count = count($term_childs);
										if($count > 0) {
											echo '<ul class="sub-menu">';

											foreach ( $term_childs as $child ) {
											    $term = get_term_by( 'id', $child, $taxonomy_name );

											    // if($term->parent == $team_lg) {
													echo '<li><a href="'.esc_url(get_term_link($term->term_id)).'">&nbsp;&nbsp;&nbsp;'.$term->name.'</a>';
														
														// cap 3
														// $term_childs = get_term_children( $term->term_id, $taxonomy_name );

														// $count = count($term_childs);
														// if($count > 0) {
														// 	echo '<ul class="sub-menu">';

														// 	foreach ( $term_childs as $child ) {
														// 		$term = get_term_by( 'id', $child, $taxonomy_name );

														// 		echo '<li><a href="'.esc_url(get_term_link($term->term_id)).'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$term->name.'</a></li>';
														// 	}
														// 	echo '</ul>';
														// }
													echo '</li>';
												// }
											}
											echo '</ul>';
										}
									echo '</li>';
								}
							?>
						</ul>
					</div>
				</div>
    		</section>
			
			<section class="widget widget-form-support">
				<h2 class="title-widget"><?php echo $sidebar_form_title; ?></h2>
				<div class="widget-form-support-content">

					<?php
						if(!empty( $sidebar_form )) {

							echo do_shortcode($sidebar_form);
					
							// Call funtion contact form
							if ( function_exists( 'wpcf7_enqueue_scripts' ) ) { wpcf7_enqueue_scripts(); }
							if ( function_exists( 'wpcf7_enqueue_styles' ) ) { wpcf7_enqueue_styles(); }
						}
					?>

				</div>
			</section>

			<section class="widget widget-related-post">
				<h2 class="title-widget"><?php echo $sidebar_select_post_title; ?></h2>
				<div class="widget-related-post-content">

		            <?php if(!empty( $sidebar_select_post )) { ?>
		            <?php
		                foreach ($sidebar_select_post as $sidebar_select_post_kq) {

		                $post_id = $sidebar_select_post_kq->ID;
		                $post_title = get_the_title($post_id);
		                $post_content = wpautop(get_the_content($post_id));
		                $post_date = get_the_date('Y/m/d',$post_id);
		                $post_link = get_post_permalink($post_id);
		                $post_image = getPostImage($post_id,"p-post");
		                $post_excerpt = cut_string(get_the_excerpt($post_id),500,'...');
		                $post_tag = get_the_tags($post_id);

		            ?>

						<div class="item">
							<figure>
								<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
									<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
								</a>
							</figure>
							<div class="info">
								<div class="title">
									<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
										<h3>
											<?php echo $post_title; ?>
										</h3>
									</a>
								</div>
							</div>
						</div>

		            <?php } ?>
		            <?php } ?>

				</div>
			</section>

			<section class="widget widget-image-ads">
				<div class="widget-image-ads-content">

					<?php if(!empty( $sidebar_image_ads )) { ?>
					<img src="<?php echo $sidebar_image_ads; ?>">
					<?php } ?>

				</div>
			</section>

		</aside>
<?php
	}
?>

<?php
	if ( is_singular() ) {
		if ( is_singular( 'product' ) ) {
?>
	    	<!-- <aside class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 sidebar sidebar-single-product">
	    		<section class="widget widget-list-post">
	    			<h2 class="title-widget"><?php echo $sidebar_single_select_product_title; ?></h2>
	    			<div class="widget-list-post-content">

			            <?php if(!empty( $sidebar_single_select_product )) { ?>
			            <?php
			                foreach ($sidebar_single_select_product as $sidebar_single_select_product_kq) {

			                $post_id = $sidebar_single_select_product_kq->ID;
			                $post_title = get_the_title($post_id);
			                $post_content = wpautop(get_the_content($post_id));
			                $post_date = get_the_date('Y/m/d',$post_id);
			                $post_link = get_post_permalink($post_id);
			                $post_image = getPostImage($post_id,"p-post");
			                $post_excerpt = cut_string(get_the_excerpt($post_id),500,'...');
			                $post_tag = get_the_tags($post_id);

			            ?>

							<div class="item">
								<figure>
									<a href="<?php echo $post_link; ?>">
										<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
									</a>
								</figure>
								<div class="info">
									<div class="title">
										<a href="<?php echo $post_link; ?>">
											<h3>
												<?php echo $post_title; ?>
											</h3>
										</a>
									</div>
									<?php
										//show_price
										$donvi = " đ";

											$oldprice = get_field('product_price_regular', $post_id);
											$price = get_field('product_price_sale', $post_id);

										echo show_price_old_price($oldprice,$price,$donvi);
									?>
								</div>
							</div>

			            <?php } ?>
			            <?php } ?>
						
	    			</div>
	    		</section>
	    	</aside> -->
<?php
		} else {
?>
	    	<aside class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12 sidebar sidebar-single-news">

	    		<section class="widget widget-list-post">
	    			<h2 class="title-widget"><?php echo $sidebar_single_select_post_title; ?></h2>
	    			<div class="widget-list-post-content">

			            <?php if(!empty( $sidebar_single_select_post )) { ?>
			            <?php
			                foreach ($sidebar_single_select_post as $sidebar_single_select_post_kq) {

			                $post_id = $sidebar_single_select_post_kq->ID;
			                $post_title = cut_string(get_the_title($post_id),80,'...');
			                $post_content = wpautop(get_the_content($post_id));
			                $post_date = get_the_date('Y/m/d',$post_id);
			                $post_link = get_post_permalink($post_id);
			                $post_image = getPostImage($post_id,"p-post");
			                $post_excerpt = cut_string(get_the_excerpt($post_id),80,'...');
			                $post_tag = get_the_tags($post_id);

			            ?>

							<div class="item">
								<figure>
									<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
										<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
									</a>
								</figure>
								<div class="info">
									<div class="title">
										<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
											<h3>
												<?php echo $post_title; ?>
											</h3>
										</a>
									</div>
								</div>
							</div>

			            <?php } ?>
			            <?php } ?>

	    			</div>
	    		</section>

	    	</aside>
<?php
		}
	}
?>

