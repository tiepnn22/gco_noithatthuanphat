var gulp          = require('gulp');               //Lệnh require tìm kiếm package gulp trong node_modules, rồi gán vào biến gulp.
var sass          = require('gulp-sass');          //Kiểm tra, biên dịch sass thành css.
var cssnano       = require('gulp-cssnano');       //Tối ưu css (Loại bỏ : khoảng trắng, dấu chấm phẩy ở thuộc tính cuối. Những selector, media queries bị bỏ trống. Những @keyframe, @font-face không được sử dụng. Những thuộc tính bị duplicate
var sourcemaps    = require('gulp-sourcemaps');    //Khi f12 ở trình duyệt, tuy chỉ có 1 file css, nhưng vẫn biết css nằm trong file nào.
var jshint        = require('gulp-jshint');        //Kiểm tra js.
var uglify        = require('gulp-uglify');        //Tối ưu js (Loại bỏ : khoảng trắng, dấu chấm phẩy ở thuộc tính cuối, comment.
var autoprefixer  = require('gulp-autoprefixer');  //Tự động thêm tiền tố css.
var concat        = require('gulp-concat');        //Nối nội dung file.
var bust          = require('gulp-buster');        //Tạo bộ nhớ cache.
var imagemin      = require('gulp-imagemin');      //Nén image.
// var webp          = require('gulp-webp');          //Nén image sang dạng webp.
var cache         = require('gulp-cache');         //Tạo cache proxy lưu trên local (ở đây dùng cho image).
var del           = require('del');                //Xoá folder, file mỗi lần gulp.
var runSequence   = require('run-sequence');       //Đảm bảo các task chạy tuần tự trong default gulp.
var watch         = require('watch');              //Theo dõi sự thay đổi của các file và tự động gulp task được gán.
var swig          = require('gulp-swig');          //Truyền file, truyền biến như trong php, laravel blade.

// Gulp css
gulp.task('css', function(){
    return gulp.src([
        'assets/css/index.scss',
        // 'assets/css/scss/**/*.scss',
    ])
    .pipe(sourcemaps.init())    //phải đứng đầu tiên
    .pipe(sass().on('error', sass.logError))
    .pipe(concat('main.css'))
    .pipe(cssnano())
    .pipe(sourcemaps.write())
    .pipe(autoprefixer({ overrideBrowserslist: [
        'last 4 versions',
        'chrome >= 14',
        // 'Firefox >= 14',
        // 'opera >= 14',
        'Safari >= 8',
        'iOS >= 8',
        'android >= 4'
        
        // 'last 3 versions',
        // 'iOS >= 8',
        // 'Safari >= 8',
        // 'ie 11',
        // 'Firefox 14',
        // 'safari 5',
        // 'ie 8',
        // 'ie 9',
        // 'opera 12.1',
        // 'ios 6',
        // 'android 4'
    ] }))
    .pipe(gulp.dest('dist/css/'))
});
// Gulp thư viện css
gulp.task('css-tool', function(){
    return gulp.src([
        'bower_components/font-awesome/css/font-awesome.min.css',
        'bower_components/bootstrap/dist/css/bootstrap.min.css',
        'bower_components/jquery.meanmenu/meanmenu.min.css',
        'bower_components/slick-carousel/slick/slick-theme.css',
        'bower_components/slick-carousel/slick/slick.css'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('tool.min.css'))
    .pipe(cssnano())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/css/'))
    .pipe(bust({relativePath: './dist/css'}))
    .pipe(gulp.dest('dist/.cache/'))
});
// Gulp js
gulp.task('js', function(){
    return gulp.src([
        'assets/js/main.js',
    ])
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js/'))
});
// Gulp thư viện js
gulp.task('js-tool', function(){
    return gulp.src([
        // 'bower_components/jquery/dist/jquery.min.js',
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/jquery.meanmenu/jquery.meanmenu.min.js',
        'bower_components/slick-carousel/slick/slick.min.js'
    ])
    .pipe(concat('tool.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('dist/js/'))
    .pipe(bust({relativePath: './dist/js'}))
    .pipe(gulp.dest('dist/.cache/'))
});
// Gulp tất cả dạng ảnh
gulp.task('images', function(){
    return gulp.src([
        'assets/images/**/*.+(png|jpg|jpeg|gif|svg)',
        // 'bower_components/slick-carousel/slick/ajax-loader.gif',
    ])
    .pipe(cache(imagemin({
        interlaced: true
    })))
    // .pipe(webp({quality: 50}))
    .pipe(gulp.dest('dist/images'))
});
// Gulp fonts
gulp.task('fonts', function() {
    return gulp.src([
        'bower_components/font-awesome/fonts/**/*',
        'assets/fonts/**/*',
    ])
    .pipe(gulp.dest('dist/fonts'))
})


// Gulp swig
gulp.task('swig', function(){
    return gulp.src([
        'views/*.swig'
    ])
    .pipe(swig({defaults: {cache: false,},}))
    .pipe(gulp.dest("dist/"))
});


// Clean file, folder mỗi lần gulp
gulp.task('clean', function() {
    return del.sync([
        // 'dist',
        'dist/css/main.css',
        'dist/js/main.js',
        'dist/fonts',
        'dist/images',
    ])
})
// Lệnh gulp default
gulp.task('default', function () {
    runSequence('clean', 'css', 'js', 'images', 'fonts', 'swig')
})
// Lệnh gulp watch (default : tên của task sẽ bị watch)
gulp.task('watch', function(){
    // gulp.watch('assets/**/*', ['default']);
    gulp.watch(['assets/**/*', 'views/**/*'], ['default']);
})


// Chia thư viện css,js ra thực thi riêng cho nhẹ task, và nén min luôn 1 lần vì nén lâu
// Clean thư viện css,js mỗi lần gulp
gulp.task('clean-tool', function() {
    return del.sync([
        'dist/css/tool.min.css',
        'dist/js/tool.min.js',
    ])
})
// Lệnh gulp gulp-tool
gulp.task('gulp-tool', function () {
    runSequence('clean-tool', 'css-tool', 'js-tool')
})

//gulp-cssmin       //Nén css.
//gulp-jsmin        //Nén js.
//gulp-browserify   //Kiem tra css.
//gulp-rename       //Đổi tên file.
//gulp-sass-glob    //Chọn nhiều file.