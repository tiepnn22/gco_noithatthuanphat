<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="canonical" href="<?php echo get_page_link_current(); ?>" />
    <title><?php echo title(); ?></title>
    <?php wp_head(); ?>

    <?php
        // add thumbnail term to share facebook
        if( is_singular( 'product' ) ) {
            $product_id = get_the_ID();
            $single_product_image   = getPostImage($product_id,"p-product");
        ?>
            <meta property="og:image" content="<?php echo $single_product_image; ?>"/>
            <!-- <meta property="og:image:width" content="380"/>
            <meta property="og:image:height" content="269"/> -->
        <?php
        }
    ?>

</head>
<body <?php body_class(); ?>>

<script>
    var ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>
    
<?php
    //Biến toàn cục cho custom post type, taxonomy
    global $glb_ctp_product;
    global $glb_tax_product;
    global $glb_ctp_video;

    $glb_ctp_product = "product";
    $glb_tax_product = "product-cat";
    $glb_ctp_video = "video";
    $glb_tax_video = "video-cat";
    //Bị xót trong file function, sidebar

    //field
    $h_top_menu = get_field('h_top_menu', 'option');
    $h_top_phone = get_field('h_top_phone', 'option');
    $h_top_fb_like = get_field('h_top_fb_like', 'option');
    $h_phone_support = get_field('h_phone_support', 'option');
?>

<header class="header">

    <div class="header-top">
        <div class="container">
            <div class="header-top-content">

                <div class="header-top-left">
                    <div class="header-top-menu">
                        <ul>
                            <?php
                                foreach ($h_top_menu as $h_top_menu_kq) {

                                $post_title = $h_top_menu_kq["link"]["title"];
                                $post_link = $h_top_menu_kq["link"]["url"];
                            ?>

                                <li>
                                    <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                                        <?php echo $post_title; ?>
                                    </a>
                                </li>

                            <?php } ?>
                        </ul>
                    </div>
                </div>

                <div class="header-top-right">
                    <div class="header-top-phone">
                        <span>Hotline: </span>
                        <a href="tel:<?php echo str_replace(' ','',$h_top_phone);?>">
                            <?php echo $h_top_phone; ?>
                        </a>
                    </div>
                    <div class="header-top-fblike">
                        <?php echo $h_top_fb_like; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="header-content">
        <div class="container">
            <div class="header-content-content">

                <?php get_template_part("resources/views/logo"); ?>

                <?php get_template_part("resources/views/search-form"); ?>

                <div class="header-phone-support">
                    <i class="fa fa-phone"></i>
                    <div class="header-phone-support-content">
                        <span>Hotline hỗ trợ</span>
                        <div class="header-phone-list">
                            <?php
                                foreach ($h_phone_support as $h_phone_support_kq) {

                                $post_phone = $h_phone_support_kq["phone"];
                            ?>

                                <a href="tel:<?php echo str_replace(' ','',$post_phone);?>">
                                    <?php echo $post_phone; ?>
                                </a> <span>-</span>

                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <nav class="menu-primary">
        <div class="container">
            <div class="main-menu">

                <?php get_template_part("resources/views/menu"); ?>

            </div>
            <div class="mobile-menu"></div>
            <?php get_template_part("resources/views/search-form"); ?>
        </div>
    </nav>

</header>

