<?php
include_once get_template_directory(). '/load/Custom_Functions.php';
include_once get_template_directory(). '/load/CTPost_CTTax.php';


/* Create CTPost */
// (title, slug_code, slug)
create_post_type("Sản phẩm","product","sanpham");
create_post_type("Video","video","video");
/* Create CTTax */
// (title, slug, slug_code, post_type)
create_taxonomy_theme("Danh mục Sản phẩm","danhmuc-sanpham","product-cat","product");
create_taxonomy_theme("Danh mục video","danhmuc-video","video-cat","video");


// Create menu Theme option use Acf Pro
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title'    => 'Theme options', // Title hiển thị khi truy cập vào Options page
        'menu_title'    => 'Theme options', // Tên menu hiển thị ở khu vực admin
        'menu_slug'     => 'theme-settings', // Url hiển thị trên đường dẫn của options page
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));
}


//Title Head Page
if (!function_exists('title')) {
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_bloginfo('name');
        }

        if (is_archive()) {
            $obj = get_queried_object();
            return $obj->name;
        }

        if (is_search()) {
            return __( 'Tìm kiếm cho', 'text_domain' ).' : ['.$_GET['s'].']';
        }

        if (is_404()) {
            return __( '404 Không tìm thấy trang', 'text_domain' );
        }

        return get_the_title();
    }
}


//Url File theme
if (!function_exists('asset')) {
    function asset($path)
    {
        return wp_slash(get_stylesheet_directory_uri() . '/dist/' . $path);
    }
}


//Url image theme
if (!function_exists('getPostImage')) {
    function getPostImage($id, $imageSize = '')
    {
        $img = wp_get_attachment_image_src(get_post_thumbnail_id($id), $imageSize);
        return (!$img) ? asset('images/no-image-wc.png') : $img[0];
    }
}


//Cut String text
if (!function_exists('cut_string')) {
    function cut_string($str,$len,$more){
        if ($str=="" || $str==NULL) return $str;
        if (is_array($str)) return $str;
            $str = trim(strip_tags($str));
        if (strlen($str) <= $len) return $str;
            $str = substr($str,0,$len);
        if ($str != "") {
            if (!substr_count($str," ")) {
              if ($more) $str .= " ...";
              return $str;
            }
            while(strlen($str) && ($str[strlen($str)-1] != " ")) {
                $str = substr($str,0,-1);
            }
            $str = substr($str,0,-1);
            if ($more) $str .= " ...";
        }
        return $str;
    }
}


// Get url page here
if (!function_exists('get_page_link_current')) {
    function get_page_link_current(){
        // Get url page here
        $page_link_current_get = sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            $_SERVER['REQUEST_URI']
        );

        // Get url, exclude url GET
        $page_link_current_strstr = strstr($page_link_current_get, '?');
        $page_link_current = str_replace( $page_link_current_strstr, '', $page_link_current_get );

        return $page_link_current;
    }
}


// Get url page template by name file (vd : template-contact.php)
if (!function_exists('get_link_page_template')) {
    function get_link_page_template($name_file){
        $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => $name_file
        ));
        
        $page_template_link = get_page_link($pages[0]->ID);

        return $page_template_link;
    }
}


//Get image Video
// if (!function_exists('getThumbnailVideo')) {
//     function getThumbnailVideo($url)
//     {
//         $strpos_url = strpos($url, '?v=') + 3;

//         $substr_url = substr( $url,  $strpos_url, 100);

//         $image_video = 'https://img.youtube.com/vi/'.$substr_url.'/mqdefault.jpg';

//         return $image_video;
//     }
// }
//Get iframe Video
if (!function_exists('getIframeVideo')) {
    function getIframeVideo($url)
    {
        $strip_tags_url = strip_tags($url);

        $iframe_video = str_replace('watch?v=','embed/',$strip_tags_url);

        return $iframe_video;
    }
}


//Format price
if (!function_exists('format_price')) {
    function format_price($money) {
        $str = "";
        if ($money != 0) {
            $num = (float)$money;
            $str = number_format($num,0,'.','.');
        }
        return $str;
    }
}
//Format price + VNĐ
if (!function_exists('format_price_donvi')) {
    function format_price_donvi($money, $donvi) {
        $str = "";
        if($money != 0) {
            $num = (float)$money;
            $str = number_format($num,0,'.','.');
            $str .= $donvi;
            $str = $str;
        }
        return $str;
    }
}
//Check old_price, price + VNĐ
if (!function_exists('show_price_old_price')) {
    function show_price_old_price($old_price, $price, $donvi2) {
        $donvi = !empty($donvi2) ? $donvi2 : '';
        $str = "";
        if($old_price > 0){
            if($price > 0 || $price != null){
                $str1 = format_price_donvi($price, $donvi);
                $str2 = format_price_donvi($old_price, $donvi);
                $str = '<div class="price">
                            <span class="price-news">'.$str1.'</span>
                            <span class="price-old">'.$str2.'</span>
                        </div>';
            } else {
                $str = '<div class="price">
                            <span class="price-news">'.format_price_donvi($old_price, $donvi).'</span>
                        </div>';
            }
        } else{
            $str = '<div class="price">
                        <a class="price-news" href="'.get_link_page_template('template-contact.php').'">Liên hệ</a>
                    </div>';
        }
        return $str;
    }
}
//Show % sale price
if (!function_exists('show_sale')) {
    function show_sale($old_price, $price) {
        if($old_price == 0){ } else {
            if($price == 0){ } else {
                $sale = (1 - ($price / $old_price))*100;
                echo '<span class="price-sale">'.ceil($sale).'%</span>';
            }
        }
    }
}


// Pagination
function paginationCustom($max_num_pages) {
    echo '<ul>';
    if ($max_num_pages > 1) {   // tổng số trang (10)
        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; // trang hiện tại (8)

        if ($max_num_pages > 9) {
            echo '<li class=""><a href="'.esc_url( get_pagenum_link( 1 ) ).'" class="">
            ...</a></li>';
        }
        if ($paged > 1) {
            echo '<li class=""><a href="'.esc_url( get_pagenum_link( $paged - 1 ) ).'" class="">
            <i class="fa fa-angle-left"></i><i class="fa fa-angle-left"></i></a></li>';
        }
        if ($paged >= 5 && $max_num_pages > 9) {
            echo '<li class=""><a href="'.esc_url( get_pagenum_link( $paged - 5 ) ).'" class="">
            -5</a></li>';
            echo '<li class=""><a href="javascript:void(0)" class="">&nbsp;</a></li>';
        }

        for($i= 1; $i <= $max_num_pages; $i++) {
            // $half_total_links = floor( 5 / 2);
            $half_total_links = 2;

            $from = $paged - $half_total_links; // trang hiện tại - 2 (8-2= 6)
            $to = $paged + $half_total_links;   // trang hiện tại + 2 (8+2 = 10)

            // if ($paged < $half_total_links) {
            //    $to += $half_total_links - $paged;
            // }
            // if ($max_num_pages - $paged < $half_total_links) {
            //     $from -= $half_total_links - ($max_num_pages - $paged) - 1;
            // }

            if ($from < $i && $i < $to) {   // $form cách $to 3 số (từ 6 đến 10 là 7,8,9)
                $class = $i == $paged ? 'active' : '';
                echo '<li class=" "><a href="'.esc_url( get_pagenum_link( $i ) ).'" class="'.$class.'">'.$i.'</a></li>';
            }
        }

        if ($paged <= $max_num_pages - 5 && $max_num_pages > 9) {
            echo '<li class=""><a href="javascript:void(0)" class="">&nbsp;</a></li>';
            echo '<li class=""><a href="'.esc_url( get_pagenum_link( $paged + 5 ) ).'" class="">
            +5</a></li>';
        }
        if ($paged + 1 <= $max_num_pages) {
            echo '<li class=""><a href="'.esc_url( get_pagenum_link( $paged + 1 ) ).'" class="">
            <i class="fa fa-angle-right"></i><i class="fa fa-angle-right"></i></a></li>';
        }
        if ($max_num_pages > 9) {
            echo '<li class=""><a href="'.esc_url( get_pagenum_link( $max_num_pages ) ).'" class="">
            ...</a></li>';
        }
    }
    echo '</ul>';
}


// Ajax Readmore_post Category
add_action('wp_ajax_Readmore_post', 'Readmore_post');
add_action('wp_ajax_nopriv_Readmore_post', 'Readmore_post');
function Readmore_post() {

    $data_offset = $_POST['data_offset'];
    $data_catid = $_POST['data_catid'];

    $data = array();

    $query =  new WP_Query( array(
            'post_type'     =>  'video',
            'tax_query'     =>  array(
                                    array(
                                            'taxonomy'  => 'video-cat',
                                            'field'     => 'id',
                                            'terms'     => $data_catid,
                                            'operator'  => 'IN'
                                    )),
            'offset'        =>  $data_offset,
            'showposts'     =>  9,
            'order'         =>  'DESC',
            'orderby'       =>  'date'
    ) );
    
    if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();

    $post_id = get_the_ID();
    $post_title = get_the_title($post_id);
    $post_content = get_the_content($post_id);
    $post_date = get_the_date('Y/m/d',$post_id);
    $post_link = get_post_permalink($post_id);
    $post_image = getPostImage($post_id,"p-product");
    $post_excerpt = cut_string(get_the_excerpt($post_id),90,'...');
    
    $video_url = getIframeVideo( get_field('video_url', $post_id) );
    $oldprice = get_field('video_price_regular', $post_id);
    $donvi = " đ";
    $price = '';
    $video_view_check = get_field('video_view', $post_id);
    $video_view = (!empty($video_view_check)) ? $video_view_check : '0';

    $data['result'] .= '
        <article class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="item" data-toggle="modal" data-target="#Modal1" data-video="'.$video_url.'">
                <figure>
                    <a href="javascript:void(0)">
                        <img src="'.$post_image.'" alt="'.$post_title.'">
                    </a>
                </figure>
                <div class="info">
                    <div class="title">
                        <a href="javascript:void(0)">
                            <h3>
                                '.$post_title.'
                            </h3>
                        </a>
                    </div>
                    <div class="meta">
                        <div class="price">
                            <span class="price-news">
                                '.show_price_old_price($oldprice,$price,$donvi).'
                            </span>
                        </div>
                        <div class="more">
                                <i class="fa fa-eye" aria-hidden="true"></i> 
                                '.$video_view.' lượt xem
                        </div>
                    </div>
                </div>
            </div>
        </article>
    ';
    
    endwhile; wp_reset_postdata(); else: $data['exit'] = 2; endif;

    echo json_encode($data);
    die();
}