<?php
    //field
    $h_top_phone = get_field('h_top_phone', 'option');
    $chat_fanpage = get_field('chat_fanpage', 'option');


    $f_one_title = get_field('f_one_title', 'option');
    $f_one_address = get_field('f_one_address', 'option');
    $f_one_phone = get_field('f_one_phone', 'option');
    $f_one_email = get_field('f_one_email', 'option');

    $f_two_title = get_field('f_two_title', 'option');
    $f_two_address = get_field('f_two_address', 'option');
    $f_two_phone = get_field('f_two_phone', 'option');
    $f_two_email = get_field('f_two_email', 'option');

    $f_support_title = get_field('f_support_title', 'option');
    $f_support_select = get_field('f_support_select', 'option');

    $f_fanpage_title = get_field('f_fanpage_title', 'option');
    $f_fanpage_iframe = get_field('f_fanpage_iframe', 'option');

    $f_socical_facebook = get_field('f_socical_facebook', 'option');
    $f_socical_youtube = get_field('f_socical_youtube', 'option');
    $f_socical_insta = get_field('f_socical_insta', 'option');
    $f_socical_twiter = get_field('f_socical_twiter', 'option');

    $f_bottom_copyright = get_field('f_bottom_copyright', 'option');

    $f_bottom_menu = get_field('f_bottom_menu', 'option');
?>

<footer class="footer">

    <div class="footer-top">
        <div class="container">

            <div class="footer-top-content">
                <div class="row">
                    <address class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 footer-column-1">
                        <div class="f-title"><?php echo $f_one_title; ?></div>
                        <div class="footer-column-content">
                            <p>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span><?php echo $f_one_address; ?></span>
                            </p>
                            <p>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <span><?php echo $f_one_phone; ?></span>
                            </p>
                            <p>
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <span><?php echo $f_one_email; ?></span>
                            </p>
                        </div>
                    </address>
                    <address class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 footer-column-2">
                        <div class="f-title"><?php echo $f_two_title; ?></div>
                        <div class="footer-column-content">
                            <p>
                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                <span><?php echo $f_two_address; ?></span>
                            </p>
                            <p>
                                <i class="fa fa-phone" aria-hidden="true"></i>
                                <span><?php echo $f_two_phone; ?></span>
                            </p>
                            <p>
                                <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <span><?php echo $f_two_email; ?></span>
                            </p>
                        </div>
                    </address>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 footer-column-3">
                        <div class="f-title"><?php echo $f_support_title; ?></div>
                        <ul>
                            <?php
                                foreach ($f_support_select as $f_support_select_kq) {

                                $post_title = $f_support_select_kq["link"]["title"];
                                $post_link = $f_support_select_kq["link"]["url"];
                            ?>

                                <li>
                                    <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                        <?php echo $post_title; ?>
                                    </a>
                                </li>

                            <?php } ?>
                        </ul>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 footer-column-4">
                        <div class="f-title"><?php echo $f_fanpage_title; ?></div>
                        <div class="f-fanpage">
                            <?php echo $f_fanpage_iframe; ?>
                        </div>
                        <div class="f-socical">
                            <ul>
                                <li>
                                    <a href="<?php echo $f_socical_facebook; ?>" target="_blank" title="facebook">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $f_socical_youtube; ?>" target="_blank" title="youtube">
                                        <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $f_socical_insta; ?>" target="_blank" title="instagram">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo $f_socical_twiter; ?>" target="_blank" title="twitter">
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom-content">
                <div class="footer-copyright">
                    <?php echo $f_bottom_copyright; ?>
                </div>
                <div class="footer-menu">
                    <ul>
                        <?php
                            foreach ($f_bottom_menu as $f_bottom_menu_kq) {

                            $post_title = $f_bottom_menu_kq["link"]["title"];
                            $post_link = $f_bottom_menu_kq["link"]["url"];
                        ?>

                            <li>
                                <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                                    <?php echo $post_title; ?>
                                </a>
                            </li>

                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</footer>

<div id="tool__society">
    <div class="tool__item">
        <a href="tel:<?php echo str_replace(' ','',$h_top_phone);?>" class="tool__icon tool__icon_tel">
            <img src="<?php echo asset('images/icon/f-phone.png'); ?>">
        </a>
        <a href="<?php echo $chat_fanpage; ?>" class="tool__icon tool__icon_mes" target="_blank">
            <img src="<?php echo asset('images/icon/f-messenger.png'); ?>">
        </a>

        <a href="javascript:void(0)" id="back-to-top" class="tool__icon tool__icon_back">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>
</div>

<!--connect.facebook-->
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v10.0&appId=182467580352571&autoLogAppEvents=1" nonce="VEdeRuel"></script>

<?php wp_footer(); ?>
</body>
</html>